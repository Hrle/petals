import turtle
import math
from typing import Callable, TypeAlias
from enum import Enum
from typing import List

default_size = 100
default_steps = 10
default_recursion = 2
default_radius = default_size

Color: TypeAlias = tuple[float, float, float]


def interpolate(start: Color, end: Color, at: float):
    return (
        start[0] + (end[0] - start[0]) * at,
        start[1] + (end[1] - start[1]) * at,
        start[2] + (end[2] - start[2]) * at,
    )


class Direction(Enum):
    left = "left"
    right = "right"


def reverse(direction: Direction):
    if direction is Direction.left:
        return Direction.right

    return Direction.left


def turn(direction: Direction, angle: float):
    if direction is Direction.left:
        turtle.left(angle)
    else:
        turtle.right(angle)


# TODO: actual function
def untouchable(radius: float, last: int, next: int):
    return (radius * 0.5) * next / last


def circle(
    direction: Direction,
    radius: float = default_radius,
    steps: int = default_steps,
    within: Callable[[int, int], None] | None = None,
    forward: Callable[[float], None] | None = None,
    color: Color | None = None,
):
    if radius < 1:
        return

    circumference = 2 * math.pi * radius
    step = circumference / steps
    angle = 360 / steps
    if color is not None:
        turtle.color(color)
        turtle.begin_fill()
    for index in range(steps):
        if forward is not None:
            forward(step)
        else:
            turtle.forward(step)
            turn(direction, angle)
    if color is not None:
        turtle.end_fill()
    if within is not None:
        for index in range(steps):
            turn(reverse(direction), angle)
            within(index, steps)
            turn(direction, angle)
            if forward is not None:
                forward(step)
            else:
                turtle.forward(step)
                turn(direction, angle)


def sin_circle(
    direction: Direction,
    ratio: float,
    radius: float = default_radius,
    steps: int = default_steps,
    within: Callable[[int, int], None] | None = None,
    forward: Callable[[float], None] | None = None,
    color: Color | None = None,
    speed: float = 1,
):
    circle(
        direction,
        radius=int(
            radius * (0.5 - (math.cos(speed * 2 * math.pi * ratio) / 2))
        ),
        steps=steps,
        within=within,
        forward=forward,
        color=color,
    )


def recurse_circle(
    radius: float = default_radius,
    steps: int | List[int] = default_steps,
    recursion: int = default_recursion,
    forward: Callable[[float], None] | None = None,
    start_color: Color | None = None,
    end_color: Color | None = None,
    make_index: Callable[[int, int], bool] | None = None,
):
    make_index = make_index or (
        lambda index, recursed: index == 0 and recursed > 1
    )

    def make_within(recursed: int, direction: Direction, radius: float):
        def within(
            index: int,
            within_steps: int,
        ):
            if recursed > recursion:
                return
            if make_index(index, recursed):
                return

            next_direction = reverse(direction)
            next_steps = (
                (steps[recursed] if recursed < len(steps) else steps[-1])
                if isinstance(steps, list)
                else steps
                if type(steps) is int
                else within_steps
            )
            next_recursion = recursed + 1
            next_radius = untouchable(radius, within_steps, next_steps)
            next_within = make_within(
                next_recursion, next_direction, next_radius
            )
            next_color = (
                interpolate(
                    start_color,
                    end_color,
                    recursed / recursion,
                )
                if start_color is not None and end_color is not None
                else None
            )

            circle(
                next_direction,
                radius=next_radius,
                steps=next_steps,
                within=next_within,
                forward=forward,
                color=next_color,
            )

        return within

    circle(
        Direction.left,
        radius=radius,
        steps=steps
        if type(steps) is int
        else steps[0]
        if isinstance(steps, list)
        else default_steps,
        forward=forward,
        within=make_within(1, Direction.left, radius),
        color=start_color,
    )
