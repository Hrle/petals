from __future__ import annotations
import turtle
from turtle import Turtle
from typing import Callable, TypeAlias, TypeVar, Generic
from enum import Enum
from dataclasses import dataclass, replace

Color: TypeAlias = tuple[float, float, float]


class Direction(Enum):
    left = "left"
    right = "right"


def reverse(direction: Direction):
    if direction is Direction.left:
        return Direction.right

    return Direction.left


def turn(direction: Direction, angle: float):
    if direction is Direction.left:
        turtle.left(angle)
    else:
        turtle.right(angle)


def interpolate(start: Color, end: Color, at: float):
    return (
        start[0] + (end[0] - start[0]) * at,
        start[1] + (end[1] - start[1]) * at,
        start[2] + (end[2] - start[2]) * at,
    )


# TODO: actual function
def untouchable(radius: float, last: int, next: int):
    return (radius * 0.5) * next / last


V = TypeVar("V")


class Value(Generic[V]):
    _key: str
    _value: V
    _context: Context | None
    _new: Callable[[Context, Value[V]], V]

    def __init__(
        self,
        key: str,
        value: V,
        new: Callable[[Context, Value[V]], V] | None = None,
        context: Context | None = None,
    ):
        self._context = context
        self._key = key
        self._value = value
        self.new = new

    @property
    def key(self) -> str:
        return self._key

    @property
    def value(self) -> V:
        return self._value

    @value.setter
    def value(self, val: V) -> None:
        self._value = val

    @property
    def new(self) -> Callable[[Context, Value[V]], V] | None:
        return self._new

    @new.setter
    def new(self, new: Callable[[Context, Value[V]], V] | None) -> None:
        self._new = new if new is not None else lambda _, val: val._value

    def next(self) -> Value[V]:
        if self._context is None:
            return Value(self._key, self._value, self._new, self._context)

        return Value(
            self._key, self._new(self._context, self), self._new, self._context
        )


default = {
    "direction": Direction.left,
    "size": 100,
    "radius": 100,
    "color": (0, 0, 0),
    "index": 0,
    "steps": 10,
    "recursed": 0,
    "recursion": 2,
    "move": lambda _: None,
    "branch": lambda _: None,
}


@dataclass
class Context:
    _turtle: Turtle = Turtle()

    direction: Value[Direction] = Value(
        "direction",
        default["direction"],
        lambda ctx, _: reverse(ctx.direction.value),
    )
    size: Value[float] = Value("size", default["size"])
    color: Value[Color] = Value("color", default["color"])

    _index: Value[int] = Value(
        "color", default["color"], lambda ctx, _: ctx.index + 1
    )
    steps: Value[int] = Value("steps", default["steps"])

    _recursed: Value[int] = Value(
        "recursed", default["recursed"], lambda ctx, _: ctx.recursed + 1
    )
    recursion: Value[int] = Value("recursion", default["recursion"])

    _move: Callable[[Context], bool] = default["move"]
    _branch: Callable[[Context], bool] = default["branch"]

    def __init__(self, turtle: Turtle = Turtle()):
        self._turtle = turtle
        pass

    @staticmethod
    def create(defaults: Context | None = None) -> Context:
        if defaults is None:
            ctx = Context()
            for key in dir(ctx):
                if not key.startswith("__"):
                    val = ctx.__dict__[key]
                    ctx.__dict__[key] = Value(
                        key, val.val, val.new, val.context
                    )
        else:
            ctx = replace(defaults)

        return ctx

    @property
    def turtle(self) -> Turtle:
        return self._turtle

    @property
    def index(self) -> int:
        return self._index.value

    @property
    def recursed(self) -> int:
        return self._recursed.value

    @property
    def move(self) -> Callable[[Context], bool]:
        return self._move

    @property
    def branch(self) -> Callable[[Context], bool]:
        return self._branch

    def next(self) -> Context:
        ctx = Context(turtle=self.turtle)
        for key in dir(ctx):
            if not key.startswith("__") and isinstance(
                ctx.__dict__[key], Value
            ):
                ctx.__dict__[key] = self.__dict__[key].next()
        return ctx

    def next_branch(self):
        ctx = Context(turtle=self.turtle)
        for key in dir(ctx):
            if not key.startswith("__") and isinstance(
                ctx.__dict__[key], Value
            ):
                ctx.__dict__[key] = ctx.__dict__[key].next()
        return ctx
