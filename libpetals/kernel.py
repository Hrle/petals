from context import Context
from typing import List, TypeAlias


Matrix: TypeAlias = List[List[int]]
Path: TypeAlias = List[int]

# this is how i thought of this:
#
# u have a matrix like this:
# 1 2 3
# 2 3 3
# 1 2 3
#
# u start here:
# (1) 2 3
# 2 3 3
# 1 2 3
#
# 1 means the next column executes 1 time (recursively):
# (1) 2 3
# 2 3 3
# 1 2 3
# so, 2 3 2 is going to execute 1, 2 and 1 times
#
# reaching the final column u will be in a position like this:
# 1 2 (3)
# (2) 3 3
# 1 (2) 3
# u have a path (indices of rows - 1, 2, 0) and an index n indicating in
# which execution of the last column u are
#
# ie the last one executes 3 times so u will have:
# 1: 1, 2, 0, 0
# 2: 1, 2, 0, 1
# 3: 1, 2, 0, 2
# that is the full path of execution


class Kernel:
    _context: Context
    _matrix: Matrix

    def __init__(self, context: Context = Context(), matrix: Matrix = Matrix()):
        self._context = context
        self._matrix = matrix

    def run(self) -> None:
        self._run(self._matrix, 0)

    def _run(self, matrix: Matrix, row: int) -> None:
        for i in range(len(matrix)):
            for j in range(matrix[row]):
                self._next(i, j)
                self._run(matrix, row + 1)

    def _next(self, matrix: Matrix, path: Path):
        self._context.next()
