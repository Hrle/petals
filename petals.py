#!/usr/bin/env python3

import random
import turtle
from argparse import ArgumentParser
from typing import List
from libpetals.circles import (
    default_steps,
    default_size,
    default_recursion,
    recurse_circle,
)

parser = ArgumentParser(description="Generate turtle petals")
parser.add_argument("--size", type=float, default=default_size)
parser.add_argument(
    "--steps",
    nargs="+",
    type=int,
    default=default_steps,
)
parser.add_argument("--recursion", type=int, default=default_recursion)
args = parser.parse_args()
size: int = args.size
steps: List[int] = args.steps
recursion: int = args.recursion

turtle.bgcolor("#021033")
turtle.speed(0)
turtle.delay(0)
recurse_circle(
    radius=size,
    steps=steps[0] if len(steps) == 1 else steps,
    recursion=recursion,
    start_color=(0.5, 0, 1),
    end_color=(0, 1, 0.5),
    make_index=lambda _1, _2: random.choice([True, False]),
)

turtle.mainloop()
